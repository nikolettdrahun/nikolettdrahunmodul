class PlayersController < ApplicationController
  def index
    @player = Player.all
  end

  def new
      @player = Player.new
  end

  def create
      @player = Player.new(player_params)

      if @player.save
          redirect_to root_path
      end
  end

  def show 
      @player = Player.find_by(id: params[:id])
  end

  def edit
      @player = Player.find_by(id: params[:id])
  end

  def update
      @player = Player.find_by(id: params[:id])
      @player.update(player_params)
      if @player.save
      redirect_to	player_path(@player.id)
    end
  end

  def destroy
  @post = Player.find_by(id: params[:id])
  @post.destroy
  redirect_to root_path
  end

  private
      def player_params
          params.require(:player).permit(:nickname, :rank, :charisma, :wisdom)
      end
end